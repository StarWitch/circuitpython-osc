==========
circuitpython-osc
==========

Open Sound Control server and client implementations in **pure python** (3.5+).

Modified to be compatible in [CircuitPython](https://github.com/adafruit/circuitpython/). WORK IN PROGRESS!

Current status
==============

- [x] It turns on
- [x] SimpleUDPClient works
- [ ] Server works
- [ ] Helper scripts work
- [ ] Tests work

Features
========

* UDP blocking/threading/forking/asyncio server implementations
* UDP client
* int, float, string, double, MIDI, timestamps, blob OSC arguments
* simple OSC address<->callback matching system
* extensive unit test coverage
* basic client and server examples

Documentation
=============

Available at https://python-osc.readthedocs.io/.

Installation
============

Copy the contents of the `/pythonosc` to your `/lib` directory on your microcontroller, and `import pythonosc.*` as in the example scripts.

Still a work in progress, everything might not work the way you'd expect.

Examples
========

Simple UDP client
-----------------

.. code-block:: python

    import random
    import time
    import wifi
    import ipaddress
    import socketpool

    from pythonosc import osc_message_builder
    from pythonosc import udp_client

    try:
        from secrets import secrets
    except ImportError:
        print("WiFi secrets are kept in secrets.py, please add them there!")
        raise

    print("Connecting to WiFi")
    wifi.radio.connect(secrets["ssid"], secrets["password"])
    print("Connected to WiFi: {0} ({1})".format(secrets["ssid"], wifi.radio.ipv4_address))

    # ping local network
    host_ip = "192.168.175.81"
    host_port = 9000
    osc_host = ipaddress.ip_address(host_ip)
    host_ping = wifi.radio.ping(osc_host)
    if host_ping:
        print("Pinging osc host ({0}): {1} ms".format(host_ip, host_ping * 1000))
    else:
        print("Could not ping osc host: {0}")

    # create the udp socket
    socketpool = socketpool.SocketPool(wifi.radio)
    client = udp_client.SimpleUDPClient(socketpool, host_ip, host_port)

    print("Sending data through UDP (IP: {0}, port: {1})".format(host_ip, host_port))

    while True:
        # send message
        client.send_message("/filter", random.random())

License?
========
Unlicensed, do what you want with it. (http://unlicense.org)
